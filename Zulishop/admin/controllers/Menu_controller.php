<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author pabhoz
 */
class Menu_controller extends BController{

    function __construct() {
        parent::__construct();
    }

    public function index()
    {
        $this->view->menu = Menus_bl::getAll();
        $this->view->rols = Rols_bl::getAll();
        $this->view->render($this,"index");
    }
    
    public function create(){
        $r = Menus_bl::create($_POST);
        print(json_encode($r));
    }
    
     public function delete(){
        $r = Menus_bl::delete($_POST);
        print(json_encode($r));
    }
    
    
     public function edit(){
        $r = Menus_bl::edit($_POST);
        print(json_encode($r));
    }

}
