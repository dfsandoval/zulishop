<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Client_controller
 *
 * @author pabhoz
 */
class Category_controller extends BController{

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->view->category = Category_bl::getAll();
        $this->view->render($this,"index");
    }
    
    public function create(){
        $r = Category_bl::create($_POST);
        print(json_encode($r));
    }
    
      public function delete(){
        $r = Category_bl::delete($_POST);
        print(json_encode($r));
    }
    
    
     public function edit(){
        $r = Category_bl::edit($_POST);
        print(json_encode($r));
    }

}
