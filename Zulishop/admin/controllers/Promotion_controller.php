<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author pabhoz
 */
class Promotion_controller extends BController{

    function __construct() {
        parent::__construct();
    }

    public function index()
    {
        $this->view->promotion = Promotion_bl::getAll();
        $this->view->render($this,"index");
    }
    
    public function create(){
        $r = Promotion_bl::create($_POST);
        print(json_encode($r));
    }
    
     public function delete(){
        $r = Promotion_bl::delete($_POST);
        print(json_encode($r));
    }
    
    
     public function edit(){
        $r = Promotion_bl::edit($_POST);
        print(json_encode($r));
    }

}
