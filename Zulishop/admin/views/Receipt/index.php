
<?php foreach ($this->receipt as $receipt) : ?>       
    <div class="modal fade in" id="modal<?php print $receipt->getId(); ?>" style="display: none; padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><font><font>X</font></font></span></button>
                    <h4 class="modal-title"><font><font>Editar Factura</font></font></h4>
                </div>
                <div class="modal-body">
                    <div class="box box-danger">

                        <form id="formulario<?php print $receipt->getId(); ?>" class="form-horizontal">
                            <div class="box-body">
                                <div class="row">

                                    <div class="col-xs-3">
                                        <label>Date </label>
                                        <input type="text" class="form-control"  id="rolInput" value="<?php print $receipt->getDate(); ?>" name="date">
                                    </div>
                                    <div class="col-xs-4">
                                        <label>Cliente </label>
                                        <input type="text" class="form-control"  id="pasInput" value="<?php print $receipt->getClient(); ?>" name="client">
                                    </div>

                                    <div class="col-xs-5">
                                        <label>Total </label>
                                        <input type="text" class="form-control"  id="emailInput" value="<?php print $receipt->getTotal(); ?>" name="total">
                                    </div>

                     
                                    <div class="col-xs-4">
                                        <label>ID </label>
                                        <input type="text" class="form-control"  id="idInput" placeholder="id" value="<?php print $receipt->getId(); ?>" name="id" disabled>
                                    </div>
                                </div>
                            </div>


                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><font><font>Cancelar</font></font></button>
                                <button type="submit"  class="btn btn-danger pull-right "><font><font>Guardar cambios</font></font></button>
                            </div>


                        </form>
                    </div>

                    <?php $form = "#formulario" . $receipt->getId();
                    $this->asyncCreation($form, "Receipt/edit", "Receipt","POST","Receipt");
                    ?>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
<?php endforeach; ?>

<?php foreach ($this->receipt as $receipt) : ?>         
    <div class="modal fade in" id="delete<?php print $receipt->getId(); ?>" style="display: none; padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><font><font>X</font></font></span></button>
                    <h4 class="modal-title"><font><font>Eliminar Factura</font></font></h4>
                </div>

                <div class="modal-body">
                    <h4 class="modal-title"><font><font>¿Esta usted seguro que desea eliminar la factura No.  <?php print $receipt->getId(); ?>?</font></font></h4> 
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><font><font>Cancelar</font></font></button>
                    <button type="button" onclick="deleteReceipt()" class="btn btn-danger pull-right" class="close" data-dismiss="modal" ><font><font>Eliminar</font></font></button>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
<?php endforeach; ?>


<br>
<div class="col-xs-12">
     <div class="box box-danger collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title"><font><font>Agregar Factura</font></font></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
        </div>
        <form id="formularioFactura" class="form-horizontal">
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-3">
                        <label>Client</label>
                        <div class="dataTables_length" id="example1_length">
                            <select name="client" aria-controls="example1" class="form-control input-sm">
                                <?php foreach ($this->clients as $client) : ?>
                                    <option><?php print $client->getId(); ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <label>Total</label>
                        <input type="text" class="form-control" id="startInput" placeholder="Total" name="total">
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-danger pull-right">Agregar</button>
            </div>
        </form>
        <?php $this->asyncCreation("#formularioFactura", "Receipt/create", "Receipt"); ?>
    </div>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Facturas de ZuliShop</h3>

            <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tbody id="idReceipt"><tr>
                        <th>ID</th>
                        <th>Date</th>
                        <th>Client</th>
                        <th>Total</th>
                    </tr>
                    <?php foreach ($this->receipt as $receipt) : ?>
                        <tr>
                            <td><?php print $receipt->getId(); ?></td>
                            <td><?php print $receipt->getDate(); ?></td>
                            <td><?php print $receipt->getClient(); ?></td>
                            <td><?php print $receipt->getTotal(); ?></td>
                             <td>                      
                                <i  class="fa fa-pencil "  name="btnEditar" data-toggle="modal" data-target="#modal<?php print $receipt->getId(); ?>" id="<?php print $receipt->getId(); ?>"></i>
                                <i> </i>
                                <i  class="fa fa-times-circle text-red " name="btn_borrar" data-toggle="modal" data-target="#delete<?php print $receipt->getId(); ?>" id="<?php print $receipt->getId(); ?>" ></i>
                            </td>
                        </tr>

                    <?php endforeach; ?>
                </tbody></table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
   

</div>


<script src="<?php print(URL); ?>public/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php print(URL); ?>public/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php print(URL); ?>public/plugins/iCheck/icheck.min.js"></script>

<script>
                var id = 0;
                var i = false;
                function deleteReceipt() {
                    i = true;

                    var data = {
                        id: id,
                        date: "r",
                        client: 1,
                        total: 0,
                    };

                    if (i == true) {
                        console.log("if data: " + data);
                        $.ajax({
                            url: "<?php print(URL); ?>Receipt/delete",
                            method: "POST",
                            data: data
                        }).done(function (r) {
                            console.log(r);
                            var r = JSON.parse(r);

                            if (r.error) {
                                alert(r.msg);
                            } else {
                                alert(r.message);
                                // document.location = "<?php print(URL); ?>";
                            }
                        });
                    }
                }

                $(function () {

                    $('#idReceipt').click(function (e) {
                        id = e.target.id;
                        console.log("id; " + id);
                    });



                });


</script>
