
<?php foreach ($this->promotion as $promotion) : ?>       
    <div class="modal fade in" id="modal<?php print $promotion->getId(); ?>" style="display: none; padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><font><font>X</font></font></span></button>
                    <h4 class="modal-title"><font><font>Editar Promoción</font></font></h4>
                </div>
                <div class="modal-body">
                    <div class="box box-danger">

                        <form id="formulario<?php print $promotion->getId(); ?>" class="form-horizontal">
                            <div class="box-body">
                                <div class="row">

                                    <div class="col-xs-3">
                                        <label>Descripción </label>
                                        <input type="text" class="form-control"  id="rolInput" value="<?php print $promotion->getDescripcion(); ?>" name="descripcion">
                                    </div>
                                    
                                   
                                    <div class="col-xs-4">
                                        <label>ID </label>
                                        <input type="text" class="form-control"  id="idInput" placeholder="id" value="<?php print $promotion->getId(); ?>" name="id" disabled>
                                    </div>
                                </div>
                            </div>


                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><font><font>Cancelar</font></font></button>
                                <button type="submit"  class="btn btn-danger pull-right "><font><font>Guardar cambios</font></font></button>
                            </div>


                        </form>
                    </div>

                    <?php $form = "#formulario" . $promotion->getId();
                    $this->asyncCreation($form, "Promotion/edit", "Promotion");
                    ?>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
<?php endforeach; ?>

<?php foreach ($this->promotion as $promotion) : ?>         
    <div class="modal fade in" id="delete<?php print $promotion->getId(); ?>" style="display: none; padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><font><font>X</font></font></span></button>
                    <h4 class="modal-title"><font><font>Eliminar Promoción</font></font></h4>
                </div>

                <div class="modal-body">
                    <h4 class="modal-title"><font><font>¿Esta usted seguro que desea eliminar la promoción  <?php print $promotion->getDescripcion(); ?>?</font></font></h4> 
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><font><font>Cancelar</font></font></button>
                    <button type="button" onclick="deletePromotion()" class="btn btn-danger pull-right" class="close" data-dismiss="modal" ><font><font>Eliminar</font></font></button>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
<?php endforeach; ?>



<br>
<div class="col-xs-12">
    <div class="box box-danger collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title"><font><font>Agregar Promoción</font></font></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
        </div>
        <form id="formularioPromo" class="form-horizontal">
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        <label>Descripción</label>
                        <input type="text" class="form-control"  id="rolInput" placeholder="Description" name="descripcion">
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-danger pull-right">Agregar</button>
            </div>
        </form>
        <?php $this->asyncCreation("#formularioPromo","Promotion/create","Promotion"); ?>
    </div>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Promociones de ZuliShop</h3>

            <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tbody id="idPromotion"><tr>
                        <th>ID</th>
                        <th>Descripcion</th>
                    </tr>
                    <?php foreach ($this->promotion as $promotion) : ?>
                        <tr>
                            <td><?php print $promotion->getId(); ?></td>
                            <td><?php print $promotion->getDescripcion(); ?></td>
                             <td>                      
                                <i  class="fa fa-pencil "  name="btnEditar" data-toggle="modal" data-target="#modal<?php print $promotion->getId(); ?>" id="<?php print $promotion->getId(); ?>"></i>
                                <i> </i>
                                <i  class="fa fa-times-circle text-red " name="btn_borrar" data-toggle="modal" data-target="#delete<?php print $promotion->getId(); ?>" id="<?php print $promotion->getId(); ?>" ></i>
                            </td>
                        </tr>

                    <?php endforeach; ?>
                </tbody></table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    
</div>


<script src="<?php print(URL); ?>public/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php print(URL); ?>public/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php print(URL); ?>public/plugins/iCheck/icheck.min.js"></script>

<script>
                var id = 0;
                var i = false;
                function deletePromocion() {
                    i = true;

                    var data = {
                        id: id,
                        descripcion: "r"
                        
                    };

                    if (i == true) {
                        console.log("if data: " + data);
                        $.ajax({
                            url: "<?php print(URL); ?>Promotion/delete",
                            method: "POST",
                            data: data
                        }).done(function (r) {
                            console.log(r);
                            var r = JSON.parse(r);

                            if (r.error) {
                                alert(r.msg);
                            } else {
                                alert(r.message);
                                // document.location = "<?php print(URL); ?>";
                            }
                        });
                    }
                }

                $(function () {

                    $('#idPromotion').click(function (e) {
                        id = e.target.id;
                        console.log("id; " + id);
                    });



                });


</script>
