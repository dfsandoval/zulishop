
<br>
<div class="col-xs-12">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Proveedores de ZuliShop</h3>

            <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tbody><tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Tel</th>
                        <th>Email</th>
                        <th>Address</th>
                    </tr>
                    <?php foreach ($this->provider as $provider) : ?>
                        <tr>
                            <td><?php print $provider->getId(); ?></td>
                            <td><?php print $provider->getName(); ?></td>
                            <td><?php print $provider->getTel(); ?></td>
                            <td><?php print $provider->getEmail(); ?></td>
                            <td><?php print $provider->getAddress(); ?></td>

                        </tr>

                    <?php endforeach; ?>
                </tbody></table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
</div>