<!-- Left side column. contains the logo and sidebar -->

 <script src="<?php print(URL); ?>public/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php print(URL); ?>public/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php print(URL); ?>public/plugins/iCheck/icheck.min.js"></script>
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar"> 
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php print(URL); ?>public/dist/img/gatito.png" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php print($this->user->getUsername()); ?></p>
        <span><i class="fa fa-user-circle text-red"></i>	&nbsp;<?php print $this->user->rolDetail->getRol();?></span>
         <i  class="fa fa-pencil "  name="btnEditar" data-toggle="modal" data-target="#modalEdit" id=""></i>
                     
      
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      
     <?php foreach ($this->menus as $key => $menu) : ?>
        <!-- treeview -->
      <li class="<?php if($key == 0) { echo "active";} ?><!--treeview-->">
          <a class="asyncLink" href="<?php print URL.$menu->getUrl(); ?>"><i class="fa fa-circle-o text-red"></i> <span><?php print $menu->getName(); ?></span></a>
        <!--a href="#">
          <i class="fa fa-heartbeat"></i> <span><?php print $menu->getName(); ?></span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a-->
        
        <!--ul class="treeview-menu">
          <li class="active">
            <a class="asyncLink" href="<?php print URL.$menu->getUrl(); ?>" >
              <i class="fa fa-circle-o"></i> Registrar
            </a>
          </li>
          
          <li>
            <a class="asyncLink" href="<?php print(URL); ?>Evaluation/search/" >
              <i class="fa fa-circle-o"></i> Buscar
            </a>
          </li>
          
        </ul-->
        
      </li>
      
      <?php endforeach; ?>
    </ul>
  </section>
  
  <!-- /.sidebar -->
</aside>


  <div class="modal fade in" id="modalEdit" style="display: none; padding-right: 17px;">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true"><font><font>X</font></font></span></button>
                <h4 class="modal-title"><font><font>Editar Datos</font></font></h4>
              </div>
              <div class="modal-body">
               <div class="box box-danger">
                   
            <form id="formularioU" class="form-horizontal">
            <div class="box-body">
              <div class="row">
                  
              <div class="col-xs-3">
                    <label>Nombre </label>
                    <input type="text" class="form-control" id="startInput" value="<?php print($this->user->getUsername()); ?>" placeholder="Start" name="username" disabled>
                  </div>  
                 <div class="col-xs-4">
                    <label>Contraseña </label>
                    <input type="text" class="form-control" id="startInput" value="<?php print($this->user->getPassword()); ?>" placeholder="Start" name="password">
                  </div>
                  <div class="col-xs-5">
                    <label>Email </label>
                    <input type="text" class="form-control" id="startInput" value="<?php print($this->user->getEmail()); ?>" placeholder="Start" name="email">
                  </div>
                  
                  <div class="col-xs-3">
                    <label>Rol </label>
                    <input type="text" class="form-control" id="startInput" value="<?php print($this->user->getRol()); ?>" placeholder="Start" name="rol" disabled>
                  </div>
                  
                  <div class="col-xs-4">
                      <label>ID </label>
                      <input type="text" class="form-control"  id="idInput" placeholder="id" value="<?php print($this->user->getId()); ?>" name="id" disabled>
                </div>
              </div>
            </div>
              
                 
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><font><font>Cancelar</font></font></button>
                <button type="submit"  class="btn btn-danger pull-right "><font><font>Guardar cambios</font></font></button>
              </div>
              
             
              </form>
        </div>
                 
         <?php  $form = "#formularioU"; 
                  $this->asyncCreation($form,"Users/edit","User"); ?>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
