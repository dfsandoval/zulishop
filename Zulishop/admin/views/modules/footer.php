<footer class="main-footer">
  <div class="pull-right hidden-xs">
    <b>Version</b> 2.3.7
  </div>
  <strong>Copyright &copy; 2016-2017 <a target="_blank" href="http://zulishop.com">Zulishop tienda</a> </strong> All rights
  reserved.
</footer>
