<script src="<?php print(URL); ?>public/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php print(URL); ?>public/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php print(URL); ?>public/plugins/iCheck/icheck.min.js"></script>
<script src="<?php print(URL); ?>public/chart/Chart.js"></script>
<br>
<div class="col-xs-12">
    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><font><font>Productos mas vendidos</font></font></h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body">
            <canvas id="myChart" style="height: 204px; width: 409px;" width="409" height="204"></canvas>
        </div>
        <!-- /.box-body -->
    </div>
</div>

<div class="col-xs-12">
    <div class="box ">
        <div class="box-header with-border">
            <h3 class="box-title"><font><font>Productos agotados</font></font></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered">
                <tbody><tr>
                        <th style="width: 10px"><font><font>ID</font></font></th>
                        <th><font><font>Producto</font></font></th>
                        <th><font><font>Umbral</font></font></th>
                        <th style="width: 40px"><font><font>Cantidad</font></font></th>
                    </tr>
                    
                     <?php foreach ($this->products as $product) : ?>
                    <tr>
                        <td><font><font><?php print $product->getId(); ?></font></font></td>
                        <td><font><font> <?php print $product->getName(); ?></font></font></td>
                        <td>
                            <div class="progress progress-xs">
                                <div class="progress-bar progress-bar-danger" style="width: <?php print $product->getQuantity(); ?>%"></div>
                            </div>
                        </td>
                        <td><span class="badge bg-red"><font><font><?php print $product->getQuantity(); ?></font></font></span></td>
                    </tr>
                    <?php endforeach; ?>
                   
                   
                </tbody></table>
        </div>
        <!-- /.box-body -->
        
    </div>
</div>
<script>
     labels = [];
     labelsN = [];
    productos = [];
    productosN = [];
    $(function () {
        $.ajax({
            url: "<?php print(URL); ?>Graphic/products",
            method: "GET"
        }).done(function (r) {
           console.log(r);

            productos = JSON.parse(r);
           
           // console.log(productos[0]);
            // select = document.getElementById('selectGenero');
            for (var i = 0; i < productos.length; i++) {
                if (i < 6) {
                      labels[i] = productos[i];
                   if (labels.length > 5) {
                       
                        infoProductosN();

                    }
                }

            }
        });
        
       
    });
     function infoProductosN() {
     $(function () {
        $.ajax({
            url: "<?php print(URL); ?>Graphic/productsNum",
            method: "GET"
        }).done(function (r) {
           
            console.log(r);
            productosN = JSON.parse(r);
           
            console.log(productosN[0]);
            for (var i = 0; i < productosN.length; i++) {
                if (i < 6) {
                   labelsN[i] = productosN[i];
                  
                    if (labelsN.length > 5) {
                            infoProductos();
                        }
                }

            }

          
        });
    });
    }
    


    function infoProductos() {
        unoV = labels[0];
        dosV = labels[1];
        tresV = labels[2];
        cuatroV = labels[3];
        cincoV = labels[4];
        seixV = labels[5];
        
        unoN = labelsN[0];
        dosN = labelsN[1];
        tresN = labelsN[2];
        cuatroN = labelsN[3];
        cincoN = labelsN[4];
        seixN = labelsN[5];
        
        var ctx = document.getElementById("myChart").getContext('2d');

        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [unoV, dosV, tresV, cuatroV, cincoV],
                datasets: [{
                        label: '# of Votes',
                        data: [unoN, dosN, tresN, cuatroN, cincoN],
                        backgroundColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
            },
            options: {
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                }
            }
        });
    }
</script>