
<?php foreach ($this->client as $client) : ?>       
    <div class="modal fade in" id="modal<?php print $client->getId(); ?>" style="display: none; padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><font><font>X</font></font></span></button>
                    <h4 class="modal-title"><font><font>Editar Cliente</font></font></h4>
                </div>
                <div class="modal-body">
                    <div class="box box-danger">

                        <form id="formulario<?php print $client->getId(); ?>" class="form-horizontal">
                            <div class="box-body">
                                <div class="row">

                                    <div class="col-xs-3">
                                        <label>Nombre </label>
                                        <input type="text" class="form-control"  id="rolInput" value="<?php print $client->getUsername(); ?>" name="username">
                                    </div>
                                    <div class="col-xs-4">
                                        <label>Contraseña </label>
                                        <input type="text" class="form-control"  id="pasInput" value="<?php print $client->getPassword(); ?>" name="password">
                                    </div>

                                    <div class="col-xs-5">
                                        <label>Email </label>
                                        <input type="text" class="form-control"  id="emailInput" value="<?php print $client->getEmail(); ?>" name="email">
                                    </div>

                                    

                                    <div class="col-xs-6">
                                        <label>ID </label>
                                        <input type="text" class="form-control"  id="idInput" placeholder="id" value="<?php print $client->getId(); ?>" name="id" disabled>
                                    </div>
                                </div>
                            </div>


                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><font><font>Cancelar</font></font></button>
                                <button type="submit"  class="btn btn-danger pull-right "><font><font>Guardar cambios</font></font></button>
                            </div>


                        </form>
                    </div>

                    <?php $form = "#formulario" . $client->getId();
                    $this->asyncCreation($form, "Client/edit", "Client");
                    ?>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
<?php endforeach; ?>

<?php foreach ($this->client as $client) : ?>         
    <div class="modal fade in" id="delete<?php print $client->getId(); ?>" style="display: none; padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><font><font>X</font></font></span></button>
                    <h4 class="modal-title"><font><font>Eliminar Cliente</font></font></h4>
                </div>

                <div class="modal-body">
                    <h4 class="modal-title"><font><font>¿Esta usted seguro que desea eliminar el cliente  <?php print $client->getUsername(); ?>?</font></font></h4> 
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><font><font>Cancelar</font></font></button>
                    <button type="button" onclick="deleteClient()" class="btn btn-danger pull-right" class="close" data-dismiss="modal" ><font><font>Eliminar</font></font></button>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
<?php endforeach; ?>

<br>
<div class="col-xs-12">
    <div class="box box-danger collapsed-box">
        <div class="box-header with-border">
            <h3 class="box-title"><font><font>Agregar Cliente</font></font></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
        </div>
        <form id="formularioCliente" class="form-horizontal">
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-4">
                        <label>Username</label>
                        <input type="text" class="form-control"  id="rolInput" placeholder="Username" name="username">
                    </div>
                    <div class="col-xs-4">
                        <label>Password</label>
                        <input type="text" class="form-control"  id="rolInput" placeholder="Password" name="password">
                    </div>
                    <div class="col-xs-4">
                        <label>Email</label>
                        <input type="text" class="form-control"  id="rolInput" placeholder="Email" name="email">
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-danger pull-right">Agregar</button>
            </div>
        </form>
        <?php $this->asyncCreation("#formularioCliente","Client/create","Client","POST","Client"); ?>
    </div>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Clientes de Zulishop</h3>

            <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tbody id="idClient"><tr>
                        <th>ID</th>
                        <th>Username</th>
                        <th>Email</th>
                    </tr>
                    <?php foreach ($this->client as $client) : ?>
                        <tr>
                            <td><?php print $client->getId(); ?></td>
                            <td><?php print $client->getUsername(); ?></td>
                            <td><?php print $client->getEmail(); ?></td>
                            <td>                      
                                <i  class="fa fa-pencil "  name="btnEditar" data-toggle="modal" data-target="#modal<?php print $client->getId(); ?>" id="<?php print $client->getId(); ?>"></i>
                                <i> </i>
                                <i  class="fa fa-times-circle text-red " name="btn_borrar" data-toggle="modal" data-target="#delete<?php print $client->getId(); ?>" id="<?php print $client->getId(); ?>" ></i>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody></table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    
</div>

<script src="<?php print(URL); ?>public/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php print(URL); ?>public/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php print(URL); ?>public/plugins/iCheck/icheck.min.js"></script>

<script>
                var id = 0;
                var i = false;
                function deleteClient() {
                    i = true;

                    var data = {
                        id: id,
                        username: "r",
                        password: "r",
                        email: "r"
                    };

                    if (i == true) {
                        console.log("if data: " + data);
                        $.ajax({
                            url: "<?php print(URL); ?>Client/delete",
                            method: "POST",
                            data: data
                        }).done(function (r) {
                            console.log(r);
                            var r = JSON.parse(r);

                            if (r.error) {
                                alert(r.msg);
                            } else {
                                alert(r.message);
                                // document.location = "<?php print(URL); ?>";
                            }
                        });
                    }
                }

                $(function () {

                    $('#idClient').click(function (e) {
                        id = e.target.id;
                        console.log("id; " + id);
                    });



                });


</script>
