
  <?php foreach ($this->rol as $rol) : ?>         </div-->
<div class="modal fade in" id="<?php print $rol->getRol(); ?>" style="display: none; padding-right: 17px;">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true"><font><font>X</font></font></span></button>
                <h4 class="modal-title"><font><font>Editar rol</font></font></h4>
              </div>
              <div class="modal-body">
               <div class="box box-danger">
                   
            <form id="formulario<?php print $rol->getRol(); ?>" class="form-horizontal">
            <div class="box-body">
              <div class="row">
                  
                <div class="col-xs-3">
                    <label>Name </label>
                 <input type="text" class="form-control"  id="rolInput" value="<?php print $rol->getRol(); ?>" name="rol">
                </div>
                <div class="col-xs-4">
                    <label>Start </label>
                    <input type="text" class="form-control" id="startInput" value="<?php print $rol->getStart(); ?>" placeholder="Start" name="start">
                  </div>
                  <div class="col-xs-5">
                      <label>ID </label>
                      <input type="text" class="form-control"  id="idInput" placeholder="id" value="<?php print $rol->getId(); ?>" name="id" disabled>
                </div>
              </div>
            </div>
              
                 
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><font><font>Cancelar</font></font></button>
                <button type="submit"  class="btn btn-danger pull-right "><font><font>Guardar cambios</font></font></button>
              </div>
              
             
              </form>
        </div>
                 
         <?php  $form = "#formulario".$rol->getRol(); 
                  $this->asyncCreation($form,"Rols/edit","Rol"); ?>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
  <?php endforeach; ?>
    
    
    <?php foreach ($this->rol as $rol) : ?>         
<div class="modal fade in" id="delete<?php print $rol->getRol(); ?>" style="display: none; padding-right: 17px;">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true"><font><font>X</font></font></span></button>
                <h4 class="modal-title"><font><font>Eliminar rol</font></font></h4>
              </div>
                
              <div class="modal-body">
                  <h4 class="modal-title"><font><font>¿Esta usted seguro que desea eliminar el rol  <?php print $rol->getRol(); ?>?</font></font></h4> 
               </div>
               
                <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><font><font>Cancelar</font></font></button>
                <button type="button" onclick="deleteRol()" class="btn btn-danger pull-right" class="close" data-dismiss="modal" ><font><font>Eliminar</font></font></button>
                 </div>
                     
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
  <?php endforeach; ?>
    </br>
    
<div class="col-xs-12">
    
       <div class="box box-danger collapsed-box">

            <div class="box-header with-border">
              <h3 class="box-title"><font><font>Crear rol</font></font></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </div>
            </div>
         <form id="formularioCool" class="form-horizontal">
            <div class="box-body">
              <div class="row">
                <div class="col-xs-3">
                 <input type="text" class="form-control"  id="rolInput" placeholder="Rol" name="rol">
                </div>
                <div class="col-xs-4">
                  <input type="text" class="form-control" id="startInput" placeholder="Start" name="start">
                  
                </div>
                
              </div>
            </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-danger pull-right">Crear</button>
              </div>
         </form>

         <?php $this->asyncCreation("#formularioCool","Rols/create","Rol","POST","Rols"); ?>
        
           
          </div>
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Roles de ZuliShop</h3>

              <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody id="idroles"><tr>
                  <th>ID</th>
                  <th>Rol</th>
                  <th>Start</th>
                   <th></th>
                </tr>
                <?php foreach ($this->rol as $rol) : ?>
                <tr >
                  <td><?php print $rol->getId(); ?></td>
                  <td><?php print $rol->getRol(); ?></td>
                  <td><?php print $rol->getStart(); ?></td>
                  <td>                      
                      <i  class="fa fa-pencil "  name="btnEditar" data-toggle="modal" data-target="#<?php print $rol->getRol(); ?>" id="<?php print $rol->getId(); ?>"></i>
                      <i> </i>
                        <i  class="fa fa-times-circle text-red " name="btn_borrar" data-toggle="modal" data-target="#delete<?php print $rol->getRol(); ?>" id="<?php print $rol->getId(); ?>" ></i>
                     </td>
                  
                </tr>
                
                <?php endforeach; ?>
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
</div>
     
        

     
   
     
          <!-- /.box -->
        
    
    
    <script src="<?php print(URL); ?>public/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php print(URL); ?>public/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php print(URL); ?>public/plugins/iCheck/icheck.min.js"></script>
    
    <script>
        var id=0;
          var i=false;
        function deleteRol() {
         i= true; 
          var data = {
        id: id,
        rol: "r",
        start: "r"
      };
      
      if(i==true){
      console.log("if data: "+data);
      $.ajax({
        url: "<?php print(URL); ?>Rols/delete",
        method: "POST",
        data: data
      }).done(function(r){
          console.log(r);
        var r = JSON.parse(r);

        if(r.error){
          alert(r.msg);
        }else{
        alert(r.message);
       // document.location = "<?php print(URL); ?>";
        }
      });
      } 
      }
     
      $(function () {
          
          $('#idroles').click(function(e){
          id = e.target.id;
          console.log("id; "+id);
          });
              
    /*  var data = {
        id: $(this).find("h3[name='id']").id(),
        rol: $(this).find("h3[name='rol']").id(),
        start: $(this).find("h3[name='start']").id()
      };*/
     
         
             
   // $('#idroles').click(function(e){
     //var id = e.target.id;
     //console.log("id; "+id);
     // var content = document.querySelector("#idInput");
     // if(content !=null){
         //  document.getElementById("idInput").setAttribute('value',id);
          //  document.getElementById("idInput").setAttribute('value',id);
    //  content.set.value=id;
    
  //}
    // var input = document.getElementsById("#idInput");
    // document.getElementById("#idInput").value = id;
     
     //  console.log("id; "+id);
 // ...
});
   
 /* function recargar() {
 document.location = "<?php print(URL); ?>Rols";
}*/

</script>
    
    

