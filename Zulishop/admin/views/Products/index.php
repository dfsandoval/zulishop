
<?php foreach ($this->products as $product) : ?>       
    <div class="modal fade in" id="modal<?php print $product->getId(); ?>" style="display: none; padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><font><font>X</font></font></span></button>
                    <h4 class="modal-title"><font><font>Editar Producto</font></font></h4>
                </div>
                <div class="modal-body">
                    <div class="box box-danger">

                        <form id="formulario<?php print $product->getId(); ?>" class="form-horizontal">
                            <div class="box-body">
                                <div class="row">

                                    <div class="col-xs-3">
                                        <label>Nombre </label>
                                        <input type="text" class="form-control"  id="rolInput" value="<?php print $product->getName(); ?>" name="name">
                                    </div>
                                    <div class="col-xs-4">
                                        <label>Precio </label>
                                        <input type="text" class="form-control"  id="pasInput" value="<?php print $product->getPrice(); ?>" name="price">
                                    </div>

                                    <div class="col-xs-5">
                                        <label>Cantidad </label>
                                        <input type="text" class="form-control"  id="emailInput" value="<?php print $product->getQuantity(); ?>" name="quantity">
                                    </div>

                                    <div class="col-xs-3">
                                        <label>Marca </label>
                                        <div class="dataTables_length" id="example1_length">
                                            <select name="brand" aria-controls="example1" class="form-control input-sm">
                                                <?php foreach ($this->brand as $brand) : ?>
                                                    <option><?php print $brand->getId(); ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-xs-4">
                                        <label>Descuento </label>
                                        <input type="text" class="form-control"  id="emailInput" value="<?php print $product->getDisscount(); ?>" name="disscount">
                                    </div>


                                    <div class="col-xs-5">
                                        <label>ID </label>
                                        <input type="text" class="form-control"  id="idInput" placeholder="id" value="<?php print $product->getId(); ?>" name="id" disabled>
                                    </div>
                                </div>
                            </div>


                            <div class="modal-footer">
                                <button type="button" id="cerrarModal" class="btn btn-default pull-left" data-dismiss="modal"><font><font>Salir</font></font></button>
                                <button  type="submit"  class="btn btn-danger pull-right " ><font><font>Guardar cambios</font></font></button>
                            </div>


                        </form>
                    </div>

                    <?php
                    $form = "#formulario" . $product->getId();
                    $this->asyncCreation($form, "Products/edit", "Product");
                    ?>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tbody><tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Brand</th>
                        <th>Disccount</th>
                    </tr>
                    <?php foreach ($this->products as $product) : ?>
                        <tr>
                            <td><?php print $product->getId(); ?></td>
                            <td><?php print $product->getName(); ?></td>
                            <td><?php print $product->getPrice(); ?></td>
                            <td><?php print $product->getQuantity(); ?></td>
                            <td><?php print $product->getBrand(); ?></td>
                            <td><?php print $product->getDisscount(); ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody></table>

            <!-- /.modal-dialog -->
        </div>
    <?php endforeach; ?>


    <?php foreach ($this->products as $product) : ?>         
        <div class="modal fade in" id="delete<?php print $product->getId(); ?>" style="display: none; padding-right: 17px;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><font><font>X</font></font></span></button>
                        <h4 class="modal-title"><font><font>Eliminar Producto</font></font></h4>
                    </div>

                    <div class="modal-body">
                        <h4 class="modal-title"><font><font>¿Esta usted seguro que desea eliminar el Producto  <?php print $product->getName(); ?>?</font></font></h4> 
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><font><font>Cancelar</font></font></button>
                        <button type="button" onclick="deleteProduct()" class="btn btn-danger pull-right" class="close" data-dismiss="modal" ><font><font>Eliminar</font></font></button>
                    </div>

                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    <?php endforeach; ?>



    <br>
    <div class="col-xs-12">

        <div class="box box-danger collapsed-box">
            <div class="box-header with-border">
                <h3 class="box-title"><font><font>Agregar Producto</font></font></h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
            <form id="formularioProducto" class="form-horizontal">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-4">
                            <label>Nombre</label>
                            <input type="text" class="form-control"  id="rolInput" placeholder="Name" name="name">
                        </div>
                        <div class="col-xs-4">
                            <label>Precio</label>
                            <input type="text" class="form-control" id="startInput" placeholder="Price" name="price">
                        </div>
                        <div class="col-xs-4">
                            <label>Cantidad</label>
                            <input type="text" class="form-control" id="startInput" placeholder="Quantity" name="quantity">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <label>Marca</label>
                            <div class="dataTables_length" id="example1_length">
                                <select name="brand" aria-controls="example1" class="form-control input-sm">
                                    <?php foreach ($this->brand as $brand) : ?>
                                        <option><?php print $brand->getId(); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <label>Descuento</label>
                            <input type="text" class="form-control" id="startInput" placeholder="Disscount" name="disscount">
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-danger pull-right">Agregar</button>
                </div>
            </form>
            <?php $this->asyncCreation("#formularioProducto", "Products/create", "Product", "POST", "Products"); ?>
        </div>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Productos de Zulishop</h3>

                <div class="box-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tbody id="idProduct"><tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Brand</th>
                            <th>Disccount</th>
                        </tr>
                        <?php foreach ($this->products as $product) : ?>
                            <tr>
                                <td><?php print $product->getId(); ?></td>
                                <td><?php print $product->getName(); ?></td>
                                <td><?php print $product->getPrice(); ?></td>
                                <td><?php print $product->getQuantity(); ?></td>
                                <td><?php print $product->getBrand(); ?></td>
                                <td><?php print $product->getDisscount(); ?></td>
                                <td>                      
                                    <i  class="fa fa-pencil "  name="btnEditar" data-toggle="modal" data-target="#modal<?php print $product->getId(); ?>" id="<?php print $product->getId(); ?>"></i>
                                    <i> </i>
                                    <i  class="fa fa-times-circle text-red " name="btn_borrar" data-toggle="modal" data-target="#delete<?php print $product->getId(); ?>" id="<?php print $product->getId(); ?>" ></i>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody></table>
            </div>
            <!-- /.box-body -->
        </div>
        <br>



    </div>

    <script src="<?php print(URL); ?>public/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php print(URL); ?>public/bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="<?php print(URL); ?>public/plugins/iCheck/icheck.min.js"></script>

    <script>



                        function dfs() {


                            //   ctxModal.data-dismiss = "modal";

                            $.ajax({
                                method: "GET",
                                url: "<?php print(URL); ?>Products"
                            }).done(function (response) {
                                $("#asyncLoadArea").html(response);
                            });
                        }
                        var id = 0;
                        var i = false;
                        function deleteProduct() {
                            i = true;

                            var data = {
                                id: id,
                                name: "r",
                                price: "r",
                                quantity: "r",
                                brand: 1,
                                disscount: 1,
                            };

                            if (i == true) {
                                console.log("if data: " + data);
                                $.ajax({
                                    url: "<?php print(URL); ?>Products/delete",
                                    method: "POST",
                                    data: data
                                }).done(function (r) {
                                    console.log(r);
                                    var r = JSON.parse(r);

                                    if (r.error) {
                                        alert(r.msg);
                                    } else {
                                        alert(r.message);
                                        // document.location = "<?php print(URL); ?>";
                                    }
                                });
                            }
                        }

                        $(function () {


                            $('#idProduct').click(function (e) {
                                id = e.target.id;
                                console.log("id; " + id);
                                // ce();
                            });

                        });

                        function ce() {
                            // var ctx = document.getElementById("cerrarModal");


                            //   $('#modal'+id).modal('hide');
                            //     $('#modal'+id).style.display=none;;


                            document.getElementById('modal' + id).style.display = "none";
                            dfs();


                        }



    </script>
