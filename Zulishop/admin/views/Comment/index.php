

<?php foreach ($this->products as $product) : ?>         
    <div class="modal fade in" id="delete<?php print $product->getId(); ?>" style="display: none; padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><font><font>X</font></font></span></button>
                    <h4 class="modal-title"><font><font>Eliminar Comentario</font></font></h4>
                </div>

                <div class="modal-body">
                    <h4 class="modal-title"><font><font>¿Esta usted seguro que desea eliminar el Comentario : <?php print $product->getDescription(); ?>?</font></font></h4> 
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><font><font>Cancelar</font></font></button>
                    <button type="button" onclick="deleteProduct()" class="btn btn-danger pull-right" class="close" data-dismiss="modal" ><font><font>Eliminar</font></font></button>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
<?php endforeach; ?>



<br>
<div class="col-xs-12">
    
    
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Comentarios de Zulishop</h3>

            <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tbody id="idProduct"><tr>
                        <th>ID</th>
                        <th>Producto</th>
                        <th>Cliente</th>
                        <th>Descripcion</th>
                       
                    </tr>
<?php foreach ($this->products as $product) : ?>
                        <tr>
                            <td><?php print $product->getId(); ?></td>
                            <td><?php print $product->getIdProduct(); ?></td>
                            <td><?php print $product->getIdCliente(); ?></td>
                            <td><?php print $product->getDescription(); ?></td>
                            
                            <td>                      
                                
                                <i  class="fa fa-times-circle text-red " name="btn_borrar" data-toggle="modal" data-target="#delete<?php print $product->getId(); ?>" id="<?php print $product->getId(); ?>" ></i>
                            </td>
                        </tr>
<?php endforeach; ?>
                </tbody></table>
        </div>
        <!-- /.box-body -->
    </div>
    <br>
   


</div>

<script src="<?php print(URL); ?>public/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php print(URL); ?>public/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php print(URL); ?>public/plugins/iCheck/icheck.min.js"></script>

<script>
    
  
   
     function dfs(){
         
        
       //   ctxModal.data-dismiss = "modal";
         
        $.ajax({
                  method:"GET",
                  url: "<?php print(URL); ?>Products"
            }).done(function(response){
                  $("#asyncLoadArea").html(response);
      });
    }
        var id=0;
          var i=false;
        function deleteProduct() {
         i= true; 
         
          var data = {
        id: id,
        name: "r",
        price: "r",
        quantity: "r",
        brand: 1,
        disscount: 1,
      };
      
      if(i==true){
      console.log("if data: "+data);
      $.ajax({
        url: "<?php print(URL); ?>Products/delete",
        method: "POST",
        data: data
      }).done(function(r){
          console.log(r);
        var r = JSON.parse(r);

        if(r.error){
          alert(r.msg);
        }else{
        alert(r.message);
       // document.location = "<?php print(URL); ?>";
        }
      });
      } 
      }
     
      $(function () {
       
          
          $('#idProduct').click(function(e){
          id = e.target.id;
          console.log("id; "+id);
           // ce();
          });
          
});

function ce(){
   // var ctx = document.getElementById("cerrarModal");
    
    
        //   $('#modal'+id).modal('hide');
         //     $('#modal'+id).style.display=none;;
    
                
            document.getElementById('modal'+id).style.display = "none";
                dfs();
            
              
          }
 


</script>