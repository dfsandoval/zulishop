-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 04-09-2017 a las 23:40:20
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `zulishop`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Brand`
--

CREATE TABLE `Brand` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `logo` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Brand`
--

INSERT INTO `Brand` (`id`, `name`, `logo`) VALUES
(1, 'Tennis', NULL),
(2, 'Studio F', NULL),
(3, 'Ela', NULL),
(4, 'Levis ', NULL),
(5, 'Arturo Calle', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Calification`
--

CREATE TABLE `Calification` (
  `id` int(11) NOT NULL,
  `idProducto` int(11) NOT NULL,
  `idCliente` int(11) NOT NULL,
  `calification` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Calification`
--

INSERT INTO `Calification` (`id`, `idProducto`, `idCliente`, `calification`) VALUES
(1, 1, 1, 4),
(2, 2, 2, 2),
(3, 3, 3, 3),
(4, 4, 1, 3),
(5, 5, 2, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Category`
--

CREATE TABLE `Category` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `parent` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Category`
--

INSERT INTO `Category` (`id`, `name`, `parent`) VALUES
(1, 'Mujer', NULL),
(2, 'Hombre', NULL),
(3, 'Infantil', NULL),
(4, 'Promociones', NULL),
(5, 'Marcas', NULL),
(6, 'Vestidos', 1),
(7, 'Blusas', 1),
(8, 'Jeans', 2),
(9, 'Camisetas', 2),
(10, 'Ropa niño', 3),
(11, 'Ropa niña', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Client`
--

CREATE TABLE `Client` (
  `id` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(300) NOT NULL,
  `email` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Client`
--

INSERT INTO `Client` (`id`, `username`, `password`, `email`) VALUES
(1, 'Pedro', '1234', 'pepito@gmail.com'),
(2, 'Andres', '1234', 'andres@gamil.com'),
(3, 'Camila', '1234', 'cami@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Comment`
--

CREATE TABLE `Comment` (
  `id` int(11) NOT NULL,
  `idProduct` int(11) NOT NULL,
  `idCliente` int(11) NOT NULL,
  `description` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Comment`
--

INSERT INTO `Comment` (`id`, `idProduct`, `idCliente`, `description`) VALUES
(1, 1, 1, 'hermoso wooooo!!!!!!'),
(2, 2, 2, 'No me gusta la ropa de esta marca'),
(3, 3, 3, 'me encanta divino'),
(4, 20, 1, 'esta muy caro!!'),
(5, 40, 2, 'quiero este producto para mi hija'),
(6, 36, 1, 'me gusta mucho este producto.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Image`
--

CREATE TABLE `Image` (
  `id` int(11) NOT NULL,
  `idProduct` int(11) NOT NULL,
  `image` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Menu`
--

CREATE TABLE `Menu` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `rol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Menu`
--

INSERT INTO `Menu` (`id`, `name`, `rol`) VALUES
(1, 'SuperMenu', 1),
(2, 'SalesMan', 3),
(3, 'Admin', 2),
(4, 'AccountMan', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `MenuItem`
--

CREATE TABLE `MenuItem` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `url` varchar(256) NOT NULL,
  `parent` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `MenuItem`
--

INSERT INTO `MenuItem` (`id`, `name`, `url`, `parent`) VALUES
(1, 'Usuarios', 'Users', NULL),
(2, 'Productos', 'Products', NULL),
(3, 'Roles', 'Rols', NULL),
(4, 'Marcas', 'Brand', NULL),
(5, 'Clientes', 'Client', NULL),
(6, 'Categorias', 'Category', NULL),
(7, 'Proveedores', 'Provider', NULL),
(8, 'Menu', 'Menu', NULL),
(9, 'Facturas', 'Receipt', NULL),
(10, 'Promociones ', 'Promotion', NULL),
(11, 'Graficos', '', NULL),
(12, 'Datos productos', 'Graphic', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Menu_x_Item`
--

CREATE TABLE `Menu_x_Item` (
  `Menu_id` int(11) NOT NULL,
  `MenuItem_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Menu_x_Item`
--

INSERT INTO `Menu_x_Item` (`Menu_id`, `MenuItem_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(2, 2),
(2, 4),
(2, 5),
(2, 6),
(2, 11),
(2, 12),
(3, 1),
(3, 2),
(3, 4),
(3, 5),
(3, 6),
(3, 10),
(3, 11),
(3, 12),
(4, 9),
(4, 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Product`
--

CREATE TABLE `Product` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `price` decimal(15,2) NOT NULL DEFAULT '0.00',
  `quantity` varchar(45) NOT NULL DEFAULT '0',
  `brand` int(11) NOT NULL,
  `disscount` int(11) DEFAULT '0',
  `image` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Product`
--

INSERT INTO `Product` (`id`, `name`, `price`, `quantity`, `brand`, `disscount`, `image`) VALUES
(1, 'Vestido sport', '79990.00', '20', 1, NULL, 'public/ropa/c61.jpg'),
(2, 'Vestido de rayas', '49900.00', '10', 1, NULL, 'public/ropa/c62.jpg'),
(3, 'Vestido flores bordado', '69900.00', '15', 2, NULL, 'public/ropa/c63.jpg'),
(4, 'kimono flores', '119900.00', '10', 3, NULL, 'public/ropa/c64.jpg'),
(5, 'Vestido manga larga flores', '109900.00', '15', 2, 50, '\r\npublic/ropa/c65.jpg'),
(6, 'Vestido negro lurex', '129900.00', '10', 1, NULL, '\r\npublic/ropa/c66.jpg'),
(7, 'Vestido largo animal print', '139900.00', '15', 2, 30, '\r\n\r\npublic/ropa/c77.jpg'),
(8, 'Blusa flores', '89900.00', '30', 3, NULL, 'public/ropa/c78.jpg'),
(9, 'Top gris con bolero', '79900.00', '30', 1, 50, 'public/ropa/c79.jpg'),
(10, 'Crop top tejido cuello v', '71900.00', '30', 2, NULL, 'public/ropa/c710.jpg'),
(11, 'Body frente cruzado', '79900.00', '20', 3, 30, 'public/ropa/c711.jpg'),
(12, 'Body con escote', '39900.00', '15', 2, NULL, 'public/ropa/c712.jpg'),
(13, 'Camisa de tiras', '79900.00', '20', 1, NULL, 'public/ropa/c713.jpg'),
(14, 'Top con encaje interno', '69900.00', '20', 3, 20, 'public/ropa/c714.jpg'),
(15, 'Blusa amarilla ', '39900.00', '20', 3, NULL, 'public/ropa/c715.jpg'),
(16, 'Jean azul desgastado', '169900.00', '15', 4, NULL, 'public/ropa/c816.jpg'),
(17, 'Jean silueta gareto ', '169900.00', '20', 1, NULL, 'public/ropa/c817.jpg'),
(18, 'Jean nudy ', '169900.00', '30', 4, 30, 'public/ropa/c818.jpg'),
(19, 'Jean skinny negro', '169900.00', '30', 5, 30, 'public/ropa/c819.jpg'),
(20, 'Jean nuddy tono oscuro', '199900.00', '15', 5, NULL, 'public/ropa/c820.jpg'),
(21, 'Jean skinny color blanco', '179900.00', '20', 1, NULL, 'public/ropa/c821.jpg'),
(22, 'Jean gris skinny', '229900.00', '20', 4, NULL, 'public/ropa/c822.jpg'),
(23, 'Jean negro con resina ', '199900.00', '20', 5, 30, 'public/ropa/c823.jpg'),
(24, 'Camisa rayas azul', '99900.00', '20', 1, NULL, 'public/ropa/c924.jpg'),
(25, 'Camisa verde en drill', '119900.00', '30', 4, NULL, 'public/ropa/c925.jpg'),
(26, 'Camisa manga larga estampada', '129900.00', '30', 4, NULL, 'public/ropa/c926.jpg'),
(27, 'Camisa de lino perilla media', '139900.00', '30', 5, NULL, 'public/ropa/c927.jpg'),
(28, 'Camisa clásica azul ', '99900.00', '20', 1, NULL, 'public/ropa/c928.jpg'),
(29, 'Camisa blanca con manitos', '119900.00', '25', 5, 30, 'public/ropa/c929.jpg'),
(30, 'Camisa con piñitas', '129900.00', '20', 1, 30, 'public/ropa/c930.jpg'),
(31, 'Polo vino con rayas', '49900.00', '20', 1, 60, 'public/ropa/c1031.jpg'),
(32, 'Polo básica azul ', '69900.00', '30', 4, NULL, 'public/ropa/c1032.jpg'),
(33, 'Polo estampado aves ', '54900.00', '30', 1, 30, 'public/ropa/c1033.jpg'),
(34, 'Camisa básica roja', '49900.00', '30', 4, NULL, 'public/ropa/c1034.jpg'),
(35, 'Bermuda estampado flores', '89900.00', '30', 1, NULL, 'public/ropa/c1035.jpg'),
(36, 'Bermuda cargo verde', '55900.00', '20', 4, NULL, 'public/ropa/c1036.jpg'),
(37, 'Pantalon color mostaza ', '89900.00', '30', 1, NULL, 'public/ropa/c1037.jpg'),
(38, 'Enterizo corto metalizado ', '82900.00', '20', 3, 30, 'public/ropa/c1138.jpg'),
(39, 'Overall denim en short', '72000.00', '30', 2, NULL, 'public/ropa/c1139.jpg'),
(40, 'Vestido manga larga con boleros', '79900.00', '30', 1, NULL, 'public/ropa/c1140.jpg'),
(41, 'Camisa con bordado en frente', '99900.00', '15', 2, 30, 'public/ropa/c1141.jpg'),
(42, 'Falda cintura resortada', '50300.00', '20', 3, NULL, 'public/ropa/c1142.jpg'),
(43, 'Falda corta con cintura', '79900.00', '15', 1, NULL, 'public/ropa/c1143.jpg'),
(44, 'Jean ruedo desflecado', '99900.00', '20', 1, NULL, 'public/ropa/c1144.jpg'),
(45, 'Camisa larga a cuadros', '89900.00', '25', 2, NULL, 'public/ropa/c1145.jpg'),
(46, 'Blusa blanca con tiras', '68900.00', '13', 1, NULL, 'public/ropa/c1146.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Product_x_Category`
--

CREATE TABLE `Product_x_Category` (
  `idProduct` int(11) NOT NULL,
  `idCategory` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Product_x_Category`
--

INSERT INTO `Product_x_Category` (`idProduct`, `idCategory`) VALUES
(1, 6),
(2, 6),
(3, 6),
(4, 6),
(5, 6),
(6, 6),
(7, 6),
(8, 7),
(9, 7),
(10, 7),
(11, 7),
(12, 7),
(13, 7),
(14, 7),
(15, 7),
(16, 8),
(17, 8),
(18, 8),
(19, 8),
(20, 8),
(21, 8),
(22, 8),
(23, 8),
(24, 9),
(25, 9),
(26, 9),
(27, 9),
(28, 9),
(29, 9),
(30, 9),
(31, 10),
(32, 10),
(33, 10),
(34, 10),
(35, 10),
(36, 10),
(37, 10),
(38, 11),
(39, 11),
(40, 11),
(41, 11),
(42, 11),
(43, 11),
(44, 11),
(45, 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Product_x_Provider`
--

CREATE TABLE `Product_x_Provider` (
  `Product_id` int(11) NOT NULL,
  `Provider_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Product_x_Receipt`
--

CREATE TABLE `Product_x_Receipt` (
  `idProduct` int(11) NOT NULL,
  `idReceipt` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(15,2) NOT NULL COMMENT 'Unit price at time of sale',
  `Promotion_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Product_x_Receipt`
--

INSERT INTO `Product_x_Receipt` (`idProduct`, `idReceipt`, `quantity`, `price`, `Promotion_id`) VALUES
(3, 1, 1, '69900.00', NULL),
(6, 3, 9, '129900.00', NULL),
(7, 3, 6, '139900.00', NULL),
(8, 1, 1, '89900.00', NULL),
(17, 2, 2, '169900.00', NULL),
(24, 2, 1, '99900.00', NULL),
(29, 3, 2, '119900.00', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Promotion`
--

CREATE TABLE `Promotion` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Promotion`
--

INSERT INTO `Promotion` (`id`, `descripcion`) VALUES
(1, 'Por la compra de 2 vestidos lleva una blusa con el 50% de descuento.'),
(2, 'Por la compra de 3 blusas lleva la 4 gratis.'),
(3, 'Por la compra de un jean mayor $200.000 lleva una camiseta.'),
(4, 'por la compra de articulos infantiles lleva el 3 con el 50% de descuento.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Provider`
--

CREATE TABLE `Provider` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `tel` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Provider`
--

INSERT INTO `Provider` (`id`, `name`, `tel`, `email`, `address`) VALUES
(1, 'Stfgroup S.A', '6850000', 'stfgroup@stf.com', 'Cra. 34 #10581, Yumbo'),
(2, 'Levi Strauss & Co.', '9876009', 'levi@levis.com', 'Cra. 34#1233, Cali'),
(3, 'Comercializadora Arturo Calle S.A.S. ', '9890979', 'ac@arturo.com', 'Bogota, Colombia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Receipt`
--

CREATE TABLE `Receipt` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `client` int(11) NOT NULL,
  `total` decimal(15,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Receipt`
--

INSERT INTO `Receipt` (`id`, `date`, `client`, `total`) VALUES
(1, '2017-09-09 09:24:15', 1, '500000.00'),
(2, '2017-09-10 11:05:15', 2, '300000.00'),
(3, '2017-06-09 09:24:15', 3, '345000.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Rol`
--

CREATE TABLE `Rol` (
  `id` int(11) NOT NULL,
  `rol` varchar(45) NOT NULL,
  `start` varchar(60) DEFAULT 'Blank/'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Rol`
--

INSERT INTO `Rol` (`id`, `rol`, `start`) VALUES
(1, 'SuperAdmin', 'Users/index'),
(2, 'Admin', 'Users/index'),
(3, 'SalesMan', 'Products/index'),
(4, 'CellarMan', 'Blank/'),
(5, 'Accountman', 'Blank/');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ShoppingCar`
--

CREATE TABLE `ShoppingCar` (
  `Client_id` int(11) NOT NULL,
  `Product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Subcategory`
--

CREATE TABLE `Subcategory` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `category` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `User`
--

CREATE TABLE `User` (
  `id` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(300) NOT NULL,
  `email` varchar(45) NOT NULL,
  `rol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `User`
--

INSERT INTO `User` (`id`, `username`, `password`, `email`, `rol`) VALUES
(1, 'jefe', '1234', 'jefe@jefe.com', 1),
(2, 'vendedor', '1234', 'test@test.com', 3),
(3, 'admin', '1234', 'admin@admin.com', 2),
(4, 'contador', '1234', 'contador@contador.com', 5);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Brand`
--
ALTER TABLE `Brand`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `Calification`
--
ALTER TABLE `Calification`
  ADD PRIMARY KEY (`id`,`idProducto`,`idCliente`),
  ADD KEY `fk_Calification_Product1_idx` (`idProducto`),
  ADD KEY `fk_Calification_Client1_idx` (`idCliente`);

--
-- Indices de la tabla `Category`
--
ALTER TABLE `Category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`),
  ADD KEY `fk_Category_Category1_idx` (`parent`);

--
-- Indices de la tabla `Client`
--
ALTER TABLE `Client`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`);

--
-- Indices de la tabla `Comment`
--
ALTER TABLE `Comment`
  ADD PRIMARY KEY (`id`,`idProduct`,`idCliente`),
  ADD KEY `fk_Comment_Product1_idx` (`idProduct`),
  ADD KEY `fk_Comment_Client1_idx` (`idCliente`);

--
-- Indices de la tabla `Image`
--
ALTER TABLE `Image`
  ADD PRIMARY KEY (`id`,`idProduct`),
  ADD KEY `fk_Image_Product1_idx` (`idProduct`);

--
-- Indices de la tabla `Menu`
--
ALTER TABLE `Menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Menu_Rol1_idx` (`rol`);

--
-- Indices de la tabla `MenuItem`
--
ALTER TABLE `MenuItem`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_MenuItem_MenuItem1_idx` (`parent`);

--
-- Indices de la tabla `Menu_x_Item`
--
ALTER TABLE `Menu_x_Item`
  ADD PRIMARY KEY (`Menu_id`,`MenuItem_id`),
  ADD KEY `fk_Menu_has_MenuItem_MenuItem1_idx` (`MenuItem_id`),
  ADD KEY `fk_Menu_has_MenuItem_Menu1_idx` (`Menu_id`);

--
-- Indices de la tabla `Product`
--
ALTER TABLE `Product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Product_Brand1_idx` (`brand`);

--
-- Indices de la tabla `Product_x_Category`
--
ALTER TABLE `Product_x_Category`
  ADD PRIMARY KEY (`idProduct`,`idCategory`),
  ADD KEY `fk_Product_has_Category_Category1_idx` (`idCategory`),
  ADD KEY `fk_Product_has_Category_Product1_idx` (`idProduct`);

--
-- Indices de la tabla `Product_x_Provider`
--
ALTER TABLE `Product_x_Provider`
  ADD PRIMARY KEY (`Product_id`,`Provider_id`),
  ADD KEY `fk_Product_has_Provider_Provider1_idx` (`Provider_id`),
  ADD KEY `fk_Product_has_Provider_Product1_idx` (`Product_id`);

--
-- Indices de la tabla `Product_x_Receipt`
--
ALTER TABLE `Product_x_Receipt`
  ADD PRIMARY KEY (`idProduct`,`idReceipt`),
  ADD KEY `fk_Product_has_Receipt_Receipt1_idx` (`idReceipt`),
  ADD KEY `fk_Product_has_Receipt_Product1_idx` (`idProduct`),
  ADD KEY `fk_Product_x_Receipt_Promotion1_idx` (`Promotion_id`);

--
-- Indices de la tabla `Promotion`
--
ALTER TABLE `Promotion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `Provider`
--
ALTER TABLE `Provider`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `Receipt`
--
ALTER TABLE `Receipt`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Receipt_Client1_idx` (`client`);

--
-- Indices de la tabla `Rol`
--
ALTER TABLE `Rol`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ShoppingCar`
--
ALTER TABLE `ShoppingCar`
  ADD PRIMARY KEY (`Client_id`,`Product_id`),
  ADD KEY `fk_Client_has_Product_Product1_idx` (`Product_id`),
  ADD KEY `fk_Client_has_Product_Client1_idx` (`Client_id`);

--
-- Indices de la tabla `Subcategory`
--
ALTER TABLE `Subcategory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`),
  ADD KEY `fk_Subcategory_Category1_idx` (`category`);

--
-- Indices de la tabla `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD KEY `fk_User_Rol_idx` (`rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `Brand`
--
ALTER TABLE `Brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `Calification`
--
ALTER TABLE `Calification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `Category`
--
ALTER TABLE `Category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `Client`
--
ALTER TABLE `Client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `Comment`
--
ALTER TABLE `Comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `Image`
--
ALTER TABLE `Image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `Menu`
--
ALTER TABLE `Menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `MenuItem`
--
ALTER TABLE `MenuItem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `Product`
--
ALTER TABLE `Product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT de la tabla `Promotion`
--
ALTER TABLE `Promotion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `Provider`
--
ALTER TABLE `Provider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `Receipt`
--
ALTER TABLE `Receipt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `Rol`
--
ALTER TABLE `Rol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `Subcategory`
--
ALTER TABLE `Subcategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `User`
--
ALTER TABLE `User`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `Calification`
--
ALTER TABLE `Calification`
  ADD CONSTRAINT `fk_Calification_Client1` FOREIGN KEY (`idCliente`) REFERENCES `Client` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Calification_Product1` FOREIGN KEY (`idProducto`) REFERENCES `Product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Category`
--
ALTER TABLE `Category`
  ADD CONSTRAINT `fk_Category_Category1` FOREIGN KEY (`parent`) REFERENCES `Category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Comment`
--
ALTER TABLE `Comment`
  ADD CONSTRAINT `fk_Comment_Client1` FOREIGN KEY (`idCliente`) REFERENCES `Client` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Comment_Product1` FOREIGN KEY (`idProduct`) REFERENCES `Product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Image`
--
ALTER TABLE `Image`
  ADD CONSTRAINT `fk_Image_Product1` FOREIGN KEY (`idProduct`) REFERENCES `Product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Menu`
--
ALTER TABLE `Menu`
  ADD CONSTRAINT `fk_Menu_Rol1` FOREIGN KEY (`rol`) REFERENCES `Rol` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `MenuItem`
--
ALTER TABLE `MenuItem`
  ADD CONSTRAINT `fk_MenuItem_MenuItem1` FOREIGN KEY (`parent`) REFERENCES `MenuItem` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Menu_x_Item`
--
ALTER TABLE `Menu_x_Item`
  ADD CONSTRAINT `fk_Menu_has_MenuItem_Menu1` FOREIGN KEY (`Menu_id`) REFERENCES `Menu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Menu_has_MenuItem_MenuItem1` FOREIGN KEY (`MenuItem_id`) REFERENCES `MenuItem` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Product`
--
ALTER TABLE `Product`
  ADD CONSTRAINT `fk_Product_Brand1` FOREIGN KEY (`brand`) REFERENCES `Brand` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Product_x_Category`
--
ALTER TABLE `Product_x_Category`
  ADD CONSTRAINT `fk_Product_has_Category_Category1` FOREIGN KEY (`idCategory`) REFERENCES `Category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Product_has_Category_Product1` FOREIGN KEY (`idProduct`) REFERENCES `Product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Product_x_Provider`
--
ALTER TABLE `Product_x_Provider`
  ADD CONSTRAINT `fk_Product_has_Provider_Product1` FOREIGN KEY (`Product_id`) REFERENCES `Product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Product_has_Provider_Provider1` FOREIGN KEY (`Provider_id`) REFERENCES `Provider` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Product_x_Receipt`
--
ALTER TABLE `Product_x_Receipt`
  ADD CONSTRAINT `fk_Product_has_Receipt_Product1` FOREIGN KEY (`idProduct`) REFERENCES `Product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Product_has_Receipt_Receipt1` FOREIGN KEY (`idReceipt`) REFERENCES `Receipt` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Product_x_Receipt_Promotion1` FOREIGN KEY (`Promotion_id`) REFERENCES `Promotion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Receipt`
--
ALTER TABLE `Receipt`
  ADD CONSTRAINT `fk_Receipt_Client1` FOREIGN KEY (`client`) REFERENCES `Client` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `ShoppingCar`
--
ALTER TABLE `ShoppingCar`
  ADD CONSTRAINT `fk_Client_has_Product_Client1` FOREIGN KEY (`Client_id`) REFERENCES `Client` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Client_has_Product_Product1` FOREIGN KEY (`Product_id`) REFERENCES `Product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `Subcategory`
--
ALTER TABLE `Subcategory`
  ADD CONSTRAINT `fk_Subcategory_Category1` FOREIGN KEY (`category`) REFERENCES `Category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `User`
--
ALTER TABLE `User`
  ADD CONSTRAINT `fk_User_Rol` FOREIGN KEY (`rol`) REFERENCES `Rol` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
