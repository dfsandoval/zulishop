<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Example_bl
 *
 * @author Pabhoz
 */
class Category_bl {
   
 public static function getCategory($id){
     $category = Category::getById($id);
     if(isset($category)){
     return $category;
     }else{
         return false;
     }
 }
 
 public static function getAll(){
     
     $categories = Category::getAll();
     foreach ( $categories as $n => $category){
         $categories[$n] = self::getCategory($category["id"]);
     }
     return $categories;
 }
 
 public static function create($data){
    return Category::instanciate($data)->create();
 }
 
   public static function delete($data){
    return Category::instanciate($data)->delete();
 }
 
 public static function edit($data){
    return Category::instanciate($data)->update();
 }

}
