<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Example_bl
 *
 * @author Pabhoz
 */
class Comments_bl {
 
 
 
 public static function getProduct($id){
     $product = Comment::getById($id);
     if(isset($product)){
     return $product;
     }else{
         return false;
     }
 }
 
 public static function getAll(){
     
     $products = Comment::getAll();
     foreach ( $products as $n => $product){
         $products[$n] = self::getProduct($product["id"]);
     }
     return $products;
 }
 
 

 
 public static function create($data){
    return Comment::instanciate($data)->create();
 }
 
  public static function delete($data){
    return Comment::instanciate($data)->delete();
 }
 
 public static function edit($data){
    return Comment::instanciate($data)->update();
 }


}
