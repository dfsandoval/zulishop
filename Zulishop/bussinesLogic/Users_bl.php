<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Example_bl
 *
 * @author Pabhoz
 */
class Users_bl {

 public static function login($username,$password){
     $usr= User::getBy("username", $username);
     if(!is_null($usr)){
        return ($usr->getPassword() == $password);
     }
     return false;
 }
 
 public static function getUser($id){
     $usr = User::getById($id);
     if(isset($usr)){
     $usr->rolDetail = Rol::getById($usr->getRol());
     return $usr;
     }else{
         return false;
     }
 }
 
 public static function getAll(){
    
     $users = User::getAll();
     foreach ( $users as $n => $user){
         $users[$n] = self::getUser($user["id"]);
     }
    return $users;
 }

 public static function getByUsername($username){
    return User::getBy("username",$username);
 }
 
 public static function create($data){
      $data["password"]=md5($data["password"]);
   $usr= User::instanciate($data)->create();
    // print_r($usr);
      
    return $usr;
 }
 

  public static function delete($data){
    return User::instanciate($data)->delete();
 }
 
 public static function edit($data){
    return User::instanciate($data)->update();
 }

 public static function encryptPassword($data){
     
     $username=$data["username"];
     $ps=md5($data["password"]);
     $em=$data["email"];
     $rol=$data["rol"];
     
     $usrEncrypt =array($username,$ps,$em,$rol);
     
     return $usrEncrypt;
 }
 

}
