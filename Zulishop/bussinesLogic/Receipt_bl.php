<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Example_bl
 *
 * @author Pabhoz
 */
class Receipt_bl {

 
 
 public static function getReceipt($id){
     $receipt = Receipt::getById($id);
     if(isset($receipt)){
    // $usr->rolDetail = Rol::getById($usr->getRol());
     return $receipt;
     }else{
         return false;
     }
 }
 
 public static function getAll(){
     $receipts = Receipt::getAll();
     foreach ( $receipts as $n => $receipt){
         $receipts[$n] = self::getReceipt($receipt["id"]);
     }
     return $receipts;
 }
 
 public static function create($data){
    return Receipt::instanciate($data)->create();
 }
 
   public static function delete($data){
    return Receipt::instanciate($data)->delete();
 }
 
 public static function edit($data){
    return Receipt::instanciate($data)->update();
 }
 

}
