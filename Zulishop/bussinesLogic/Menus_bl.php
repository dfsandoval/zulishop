<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Example_bl
 *
 * @author Pabhoz
 */
class Menus_bl {
 
 public static function getUserMenus($rol){
     
     $menu = Menu::getBy("rol", $rol);
     if(!isset($menu)){ return []; }
     $menus = Menu::whereR("MenuItem_id", "Menu_id", $menu->getId(), "Menu_x_Item");

     foreach ($menus as $key => $menu) {
         $menus[$key] = MenuItem::getById($menu["MenuItem_id"]);
     }
    return $menus;

}

 public static function getMenu($id){
     $menu = Menu::getById($id);
     if(isset($menu)){
     $menu->rolDetail = Rol::getById($menu->getRol());
     return $menu;
     }else{
         return false;
     }
 }

public static function getAll(){
     $menus = Menu::getAll();
     foreach ( $menus as $n => $menu){
         $menus[$n] = self::getMenu($menu["id"]);
     }
     return $menus;
 }
 
 public static function create($data){
    return Menu::instanciate($data)->create();
 }
 
   public static function delete($data){
    return Menu::instanciate($data)->delete();
 }
 
 public static function edit($data){
    return Menu::instanciate($data)->update();
 }
 
}