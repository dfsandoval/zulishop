<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Example_bl
 *
 * @author Pabhoz
 */
class Promotion_bl {
 


 public static function getPromotion($id){
     $promotion = Promotion::getById($id);
     if(isset($promotion)){
    // $menu->rolDetail = Rol::getById($menu->getRol());
     return $promotion;
     }else{
         return false;
     }
 }

public static function getAll(){
     $promotions = Promotion::getAll();
     foreach ( $promotions as $n => $promotion){
         $promotions[$n] = self::getPromotion($promotion["id"]);
     }
     return $promotions;
 }
 
 public static function create($data){
    // print_r($data);
    return Promotion::instanciate($data)->create();
 }
 
   public static function delete($data){
    return Promotion::instanciate($data)->delete();
 }
 
 public static function edit($data){
    return Promotion::instanciate($data)->update();
 }
 
}