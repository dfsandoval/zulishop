<?php $this->loadModule("head"); ?>
<body>



<div class="login-box">
  <div class="login-logo">
    
                        <a class="navbar-brand" href="<?php print(URL); ?>Index" style=" width:100%; height: auto;" ><img id="logoP" src="public/Logo.png" alt="Page Logo" style="margin:0 auto;"></a>
                   
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <form id="loginForm" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="username" placeholder="username">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="email" placeholder="email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-12">
            <button type="submit" class="btn btn-primary btn-block btn-flat" style=" background-color: #ff4948; border-color: #ff4948; width:100%; margin: 0 auto;">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    
  </div>
  <!-- /.login-box-body -->
</div>

              
            
       
         <?php $this->asyncCreation("#loginForm","Registro/create","Client"); ?>
    <script src="<?php print(URL); ?>public/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php print(URL); ?>public/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php print(URL); ?>public/plugins/iCheck/icheck.min.js"></script>
    
    <script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });

    $("#loginForm").submit(function(e){
      e.preventDefault();
      var data = {
        usuario: $(this).find("input[name='usuario']").val(),
        password: $(this).find("input[name='password']").val()
      };
      $.ajax({
        url: "<?php print(URL); ?>Login/login",
        method: "POST",
        data: data
      }).done(function(r){
          console.log(r);

        if(r.error){
          alert(r.msg);
        }else{
          //alert(r.msg);
        document.location = "<?php print(URL); ?>";
        }
      });
    });
  });

</script>
</body>
<?php $this->loadModule("footer"); ?>
</html>