<body>
<?php $this->loadModule("head"); ?>


<div class="login-box">
  <div class="login-logo">
    
                        <a class="navbar-brand" href="<?php print(URL); ?>Index" style=" width:100%; height: auto;" ><img id="logoP" src="public/Logo.png" alt="Page Logo" style="margin:0 auto;"></a>
                   
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <form id="loginForm" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="username" placeholder="username">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <div class="icheckbox_square-blue" aria-checked="false" aria-disabled="false" style="position: relative;">
                  <input type="checkbox" style="position: absolute; visibility: hidden;">
                  <ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;">
                  </ins>
              </div> Remember Me
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
            <button type="submit" class="btn btn-primary btn-block btn-flat" style=" background-color: #ff4948; border-color: #ff4948;">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
    
    <a href="<?php print(URL); ?>Registro" class="text-center">Register a new membership</a>

  </div>
  <!-- /.login-box-body -->
</div>


    <script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });

    $("#loginForm").submit(function(e){
      e.preventDefault();
      var data = {
        usuario: $(this).find("input[name='usuario']").val(),
        password: $(this).find("input[name='password']").val()
      };
      $.ajax({
        url: "<?php print(URL); ?>Login/login",
        method: "POST",
        data: data
      }).done(function(r){
          console.log(r);
        var r = JSON.parse(r);

        if(r.error){
          alert(r.msg);
        }else{
          //alert(r.msg);
        document.location = "<?php print(URL); ?>";
        }
      });
    });
  });

</script>
</body>
<?php $this->loadModule("footer"); ?>
</html>