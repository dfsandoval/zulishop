<?php $this->loadModule("head"); ?>
<body>
<?php $this->loadModule("header"); ?>
    
    <div class="collapse navbar-collapse navHeaderCollapse">
                <ul class="nav navbar-nav ">
                    <li ><a href="#">HOME</a></li>
                    <li class="dropdown active ">
                        <a href="<?php print (URL); ?>Mujer" class="dropdown-toggle" data-toggle = "dropdown">MUJER<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Blusas</a></li>
                            <li><a href="#">Vestidos</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="<?php print (URL); ?>Hombre" class="dropdown-toggle" data-toggle = "dropdown">HOMBRE<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Jeans</a></li>
                            <li><a href="#">Camisas</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="<?php print (URL); ?>Infantil" class="dropdown-toggle" data-toggle = "dropdown">INFANTIL<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Ropa niño</a></li>
                            <li><a href="#">Ropa niña</a></li>
                        </ul>
                    </li>
                    <li "><a href="<?php print (URL); ?>Promocion">PROMO</a></li>
                    <li><a href="<?php print (URL); ?>Marca">MARCAS</a></li>

                </ul>
            </div>


    </header>

<!--CARD-->
<div class="content">
    <div class="row">
        <div class="col-md-2">
            
           <!-- <?php $this->loadModule("side"); ?>-->

        </div>

        
        <div class="col-md-8">

            <div class="rol" style="margin-right: 60px;">
                <?php foreach ($this->products as $products) : ?>
                    <div class="col-md-4">
                        <div class="profile text-center">
                            <img class="img-responsive" src="<?php print $products->getImage(); ?>" style="margin: 0 auto;">
    <!--                        <img class="img-responsive" src="public/c6_v1.jpg " style="margin: 0 auto;">-->
                            <div class="profile-info">

                                <div class="red-bar" style="width: 100%;" >
                                    <img src="public/car.png" style="margin: 0 auto;">
                                </div>
                                <div style="height: auto;width: 100%;">
                                    <div class="info-title"><?php print $products->getName(); ?></div>
                                    <div class="info-price"><?php print $products->getPrice(); ?></div>
                                    <div class="stars">
                                        <div id="stars" class="starrr">
                                            <span class="glyphicon .glyphicon-star-empty glyphicon-star-empty"></span>
                                            <span class="glyphicon .glyphicon-star-empty glyphicon-star-empty"></span>
                                            <span class="glyphicon .glyphicon-star-empty glyphicon-star-empty"></span>
                                            <span class="glyphicon .glyphicon-star-empty glyphicon-star-empty"></span>
                                            <span class="glyphicon .glyphicon-star-empty glyphicon-star-empty"></span>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                <?php endforeach; ?>
            </div>

        </div>
    </div>
</div>
<br><br>



</body>

<?php $this->loadModule("footer"); ?>
</html>