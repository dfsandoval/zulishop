<?php $this->loadModule("head"); ?>
<body>
<?php $this->loadModule("header"); ?>
</header>
    <div class="container" style="width: 100%;margin: 0 auto;">
        <?php foreach ($this->cliente as $clientes) : ?>
        <div class="col-md-12" style="">
            <div class="box box-danger">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">

              <h3 class="profile-username text-center"><?php print $clientes->getUsername(); ?></h3>

              <p class="text-muted text-center">Software Engineer</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Followers</b> <a class="pull-right"><?php print $clientes->getEmail(); ?></a>
                </li>
                <li class="list-group-item">
                  <b>Following</b> <a class="pull-right">543</a>
                </li>
                <li class="list-group-item">
                  <b>Friends</b> <a class="pull-right">13,287</a>
                </li>
              </ul>

              <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <?php endforeach; ?>

    
        
    </div>

</body>
<?php $this->loadModule("footer"); ?>
</html>