<?php $this->loadModule("head"); ?>
<body>
<?php $this->loadModule("header"); ?>

</header>


<div class="container">
<section class="invoice">
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
              <img src="public/cart2.png"> CARRITO DE COMPRAS
          </h2>
        </div>
      </div>
      
       <!--/.row -->

       <!--Table row-->
      <div class="row">
          <div class="col-xs-12 table-responsive"style="padding: 0;">
            <table class="table table-striped">
            <thead>
                <tr>
              <th class="red-b">Cantidad</th>
              <th class="red-b">Producto</th>
              <th class="red-b">Precio</th>
              <th class="red-b">Subtotal</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>1</td>
              <td>Call of Duty</td>
              <td>455-981-221</td>
              <td>$64.50</td>
            </tr>
            <tr>
              <td>1</td>
              <td>Need for Speed IV</td>
              <td>247-925-726</td>
              <td>$50.00</td>
            </tr>
            <tr>
              <td>1</td>
              <td>Monsters DVD</td>
              <td>735-845-642</td>
              <td>$10.70</td>
            </tr>
            <tr>
              <td>1</td>
              <td>Grown Ups Blue Ray</td>
              <td>422-568-642</td>
              <td>$25.99</td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>

      <div class="row">
        <div class="col-xs-8">
        </div>
        <div class="col-xs-4">
          <p class="lead">Cantidad 2/22/2014</p>

          <div class="table-responsive">
            <table class="table">
              <tbody><tr>
                <th style="width:50%">Subtotal:</th>
                <td>$250.30</td>
              </tr>
              <tr>
                <th>Tax (9.3%)</th>
                <td>$10.34</td>
              </tr>
              <tr>
                <th>Shipping:</th>
                <td>$5.80</td>
              </tr>
              <tr id="total">
                <th >Total:</th>
                <td >$265.24</td>
              </tr>
            </tbody></table>
          </div>
        </div>
        
         
      </div>
      

       
      <div class="row no-print">
        <div class="col-xs-12">
            <a href="<?php print (URL);?>Index" class="btn btn-default cute">Elegir más Productos</a>
          <button type="button" class="btn btn-danger pull-right"> Finalizar Compra
          </button>
        </div>
      </div>
</section></div>



</body>

<?php $this->loadModule("footer"); ?>
</html>