<?php $this->loadModule("head"); ?>
<body>
<?php $this->loadModule("header"); ?>

    <div class="collapse navbar-collapse navHeaderCollapse">
                <ul class="nav navbar-nav ">
                    <li ><a href="#">HOME</a></li>
                    <li class="dropdown ">
                        <a href="<?php print (URL); ?>Mujer" class="dropdown-toggle" data-toggle = "dropdown">MUJER<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Blusas</a></li>
                            <li><a href="#">Vestidos</a></li>
                        </ul>
                    </li>
                    <li class="dropdown active">
                        <a href="<?php print (URL); ?>Hombre" class="dropdown-toggle" data-toggle = "dropdown">HOMBRE<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Jeans</a></li>
                            <li><a href="#">Camisas</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="<?php print (URL); ?>Infantil" class="dropdown-toggle" data-toggle = "dropdown">INFANTIL<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Ropa niño</a></li>
                            <li><a href="#">Ropa niña</a></li>
                        </ul>
                    </li>
                    <li "><a href="<?php print (URL); ?>Promocion">PROMO</a></li>
                    <li><a href="<?php print (URL); ?>Marca">MARCAS</a></li>

                </ul>
            </div>


    </header>

<!--Carrousel-->
<div class="container"> 

    <div id="myCarousel" class="carousel slide" data-ride="carousel">
<!--         Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

<!--         Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <img src="public/banner.jpg" alt="Los Angeles" style="width:100%;">
            </div>

            <div class="item">
                <img src="public/man.jpg" alt="Chicago" style="width:100%;">
            </div>

            <div class="item">
                <img src="public/her.jpg" alt="New york" style="width:100%;">
            </div>
        </div>

<!--         Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>

<!--PROMOS-->

<br><br>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <a href="<?php print(URL); ?>Promociones">
                <img src="public/promo1.jpg" alt="promo" style="width:100%"></a>
        </div>
        <div class="col-md-4">
            <a href="<?php print(URL); ?>Infantil">
                <img src="public/4.jpg" alt="Fjords" style="width:100%">
            </a>
        </div>
    </div>
</div>
<br><br>
<!--
CATEGORIAS-->
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <a href="<?php print(URL); ?>Mujer">
                <img src="public/13.jpg" alt="her" style="width:100%;"></a>
        </div>
        <div class="col-md-4">
            <a href="<?php print(URL); ?>Infantil">
                <img src="public/12.jpg" alt="her" style="width:100%;"></a>
        </div>
        <div class="col-md-4">
            <a href="<?php print(URL); ?>Hombre">
                <img src="public/11.jpg" alt="her" style="width:100%;"></a>
        </div>
    </div>
</div><br><br>

<!--BUNDLE-->
<div class="container">
    <div class="row">
        <a href="<?php print(URL); ?>Promociones" >
            <img src="public/bundle.jpg" alt="her" style="width:100%;"></a>
    </div>
</div>
<br><br>
<!--
CARD-->
<!--<div class="container">
    <div class="col-lg-12">
        <div class="col-md-4">
            <div class="profile-card text-center">

                <img class="img-responsive" src="public/c6_v1.jpg " style="margin: 0 auto;">
                <div class="profile-info">

                    <div class="red-bar" style="width: 100%;" >
                        <img src="public/car.png" style="margin: 0 auto;">
                    </div>
                    <div style="height: auto;width: 100%;">
                        <div class="info-title">Camiseta</div>
                        <div class="info-price">$39.900</div>
                        <div class="stars">
                            <div id="stars" class="starrr"><span class="glyphicon .glyphicon-star-empty glyphicon-star-empty"></span><span class="glyphicon .glyphicon-star-empty glyphicon-star-empty"></span><span class="glyphicon .glyphicon-star-empty glyphicon-star-empty"></span><span class="glyphicon .glyphicon-star-empty glyphicon-star-empty"></span><span class="glyphicon .glyphicon-star-empty glyphicon-star-empty"></span></div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="col-md-4">
            <div class="profile-card text-center">

                <img class="img-responsive" src="public/c6_v1.jpg " style="margin: 0 auto;">
                <div class="profile-info">

                    <div class="red-bar" style="width: 100%;" >
                        <img src="public/car.png" style="margin: 0 auto;">
                    </div>
                    <div style="height: auto;width: 100%;">
                        <div class="info-title">Camiseta</div>
                        <div class="info-price">$39.900</div>
                        <div class="stars">
                            <div id="stars" class="starrr"><span class="glyphicon .glyphicon-star-empty glyphicon-star-empty"></span><span class="glyphicon .glyphicon-star-empty glyphicon-star-empty"></span><span class="glyphicon .glyphicon-star-empty glyphicon-star-empty"></span><span class="glyphicon .glyphicon-star-empty glyphicon-star-empty"></span><span class="glyphicon .glyphicon-star-empty glyphicon-star-empty"></span></div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="col-md-4">
            <div class="profile-card text-center">

                <img class="img-responsive" src="public/c6_v1.jpg " style="margin: 0 auto;">
                <div class="profile-info">

                    <div class="red-bar" style="width: 100%;" >
                        <img src="public/car.png" style="margin: 0 auto;">
                    </div>
                    <div style="height: auto;width: 100%;">
                        <div class="info-title">Camiseta</div>
                        <div class="info-price">$39.900</div>
                        <div class="stars">
                            <div id="stars" class="starrr"><span class="glyphicon .glyphicon-star-empty glyphicon-star-empty"></span><span class="glyphicon .glyphicon-star-empty glyphicon-star-empty"></span><span class="glyphicon .glyphicon-star-empty glyphicon-star-empty"></span><span class="glyphicon .glyphicon-star-empty glyphicon-star-empty"></span><span class="glyphicon .glyphicon-star-empty glyphicon-star-empty"></span></div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
<br><br>-->

<!--
CARRITO-------------------------------------------SHOPPING CART
<div class="container">
<section class="invoice">
       title row 
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
              <img src="public/cart2.png"> CARRITO DE COMPRAS
          </h2>
        </div>
         /.col 
      </div>
      
       /.row 

       Table row 
      <div class="row">
          <div class="col-xs-12 table-responsive"style="padding: 0;">
            <table class="table table-striped">
            <thead>
                <tr>
              <th class="red-b">Cantidad</th>
              <th class="red-b">Producto</th>
              <th class="red-b">Precio</th>
              <th class="red-b">Subtotal</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>1</td>
              <td>Call of Duty</td>
              <td>455-981-221</td>
              <td>$64.50</td>
            </tr>
            <tr>
              <td>1</td>
              <td>Need for Speed IV</td>
              <td>247-925-726</td>
              <td>$50.00</td>
            </tr>
            <tr>
              <td>1</td>
              <td>Monsters DVD</td>
              <td>735-845-642</td>
              <td>$10.70</td>
            </tr>
            <tr>
              <td>1</td>
              <td>Grown Ups Blue Ray</td>
              <td>422-568-642</td>
              <td>$25.99</td>
            </tr>
            </tbody>
          </table>
        </div>
         /.col 
      </div>
       /.row 

      <div class="row">
         accepted payments column 
        <div class="col-xs-8">
        </div>
         /.col 
        <div class="col-xs-4">
          <p class="lead">Cantidad 2/22/2014</p>

          <div class="table-responsive">
            <table class="table">
              <tbody><tr>
                <th style="width:50%">Subtotal:</th>
                <td>$250.30</td>
              </tr>
              <tr>
                <th>Tax (9.3%)</th>
                <td>$10.34</td>
              </tr>
              <tr>
                <th>Shipping:</th>
                <td>$5.80</td>
              </tr>
              <tr id="total">
                <th >Total:</th>
                <td >$265.24</td>
              </tr>
            </tbody></table>
          </div>
        </div>
        
         /.col 
      </div>
       /.row 

       this row will not appear when printing 
      <div class="row no-print">
        <div class="col-xs-12">
            <a href="./views/Mujer/index.php" target="_blank" class="btn btn-default cute">Elegir más Productos</a>
          <button type="button" class="btn btn-danger pull-right"> Finalizar Compra
          </button>
        </div>
      </div>
</section></div>-->





<?php $this->loadModule("footer"); ?>
</html>