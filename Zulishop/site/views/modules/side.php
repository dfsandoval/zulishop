
  <div class="box box-danger">
         
        <div class="box-header with-border">
            <h3 class="box-title"><font><font>Agregar Usuario</font></font></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
        </div>
        <form id="formularioUser" class="form-horizontal">
            <div class="box-body" style="display: block;">
                <div class="row">
                    <div class="col-xs-3">
                        <label>Username </label>
                        <input type="text" class="form-control" id="rolInput" placeholder="Username" name="username">
                    </div>
                    <div class="col-xs-3">
                        <label>Password </label>
                        <input type="text" class="form-control" id="startInput" placeholder="Password" name="password">
                    </div>
                    <div class="col-xs-3">
                        <label>Email </label>
                        <input type="text" class="form-control" id="startInput" placeholder="Email" name="email">
                    </div>
                    <div class="col-xs-2">
                        <label>Rol </label>
                        <div class="dataTables_length" id="example1_length">
                        <select name="rol" aria-controls="example1" class="form-control input-sm">
                                                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                            </select>
                        </div>
                    </div>
            </div>
            </div>
            <div class="box-footer" style="display: block;">
                <button type="submit" class="btn btn-danger pull-right">Crear</button>
            </div>
        </form>
          <script>
                            $(function(){
                              $("#formularioUser").submit(function(e){
                                e.preventDefault();
                                var inputs = $(this)[0].querySelectorAll("input,select,radio,textarea");
                                //console.log(inputs);
                                var data = {};
                                for (var i = 0; i < inputs.length; i++) {
                                  data[inputs[i].name] = inputs[i].value;
                                };
                                delete data[""];
                                delete data["undefined"];
                                console.log(data);
                                $.ajax({
                                  url:"http://localhost/web2/zulishop/Zulishop/admin/Users/create",
                                  method:"POST",
                                  data:data
                                }).done(function(r){
                                    console.log(r);
                                  if(JSON.parse(r)){
                                    r = JSON.parse(r);
                                    if(r.error == 0){
                                      alert("User creado con éxito.");
                                         
                                      
                                    }else{
                                      alert("Error en la creación del User, intente nuevamente o comuniquese con soporte");
                                    }
                                  }else{
                                      alert("Error desconocido, intente nuevamente o comuniquese con soporte");
                                    }
                                });
                              });
                            });
                </script>
        
    </div>

