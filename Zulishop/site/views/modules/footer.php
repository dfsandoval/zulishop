<footer class="footer-distributed">

			<div class="footer-left">
                            <img src="public/Logo.png" style="width: auto; height: 60px;">

				<p class="footer-links">
					<a href="<?php print (URL); ?>Index">Home</a>
					·
					<a href="<?php print (URL); ?>Mujer">Mujer</a>
					·
					<a href="<?php print (URL); ?>Hombre">Hombre</a>
					·
					<a href="<?php print (URL); ?>Infantil">Infantil</a>
					·
					<a href="<?php print (URL); ?>Promociones">Promos</a>
					·
					<a href="<?php print (URL); ?>Marcas">Marcas</a>
				</p>

				<p class="footer-company-name">Zulishop &copy; 2017</p>
			</div>

			<div class="footer-center">

				<div>
					<i class="fa fa-map-marker"></i>
					<p><span>Calle 27norte#6n-38</span> Cali, Colombia</p>
				</div>

				<div>
					<i class="fa fa-phone"></i>
					<p>+1 555 123456</p>
				</div>

				<div>
					<i class="fa fa-envelope"></i>
					<p><a href="mailto:zulishop@shop.com">zulishop@shop.com</a></p>
				</div>

			</div>

			<div class="footer-right">

				<p class="footer-company-about">
					<span>Nosotros</span>
					Nuestro compromiso con la moda e innovación es constante, por eso ofrecemos prendas que están a la vanguardia en diseño y en procesos de producción.
				</p>

				<div class="footer-icons">

					<a href="https://facebook.com"><img src="public/fb.png"></a>
					<a href="https://twitter.com/"><img src="public/tw.png"></a>
					<a href="https://co.linkedin.com/"><img src="public/ln.png"></a>
                                        <a href="https://bitbucket.org/dfsandoval/zulishop"><img src="public/bb.png"></a>

				</div>

			</div>

		</footer>