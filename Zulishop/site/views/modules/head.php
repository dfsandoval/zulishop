<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php print($this->title); ?></title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="../admin/public/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <script src="../admin/public/plugins/iCheck/icheck.min.js"></script>
        <!--script src="../admin/public/plugins/ParallaxJS/parallax.js-1.4.2/parallax.min.js"></script-->
        <script src="../admin/public/plugins/JQuery/jquery-2.2.3.min.js"></script>
        <script src="../admin/public/bootstrap/js/bootstrap.js"></script>
        <script src="../admin/public/bootstrap/js/bootstrap.min.js"></script>


        <link rel="stylesheet" href="../admin/public/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="../admin/public/plugins/font-awesome-4.7.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="../admin/public/plugins/ionicons-2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="../admin/public/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="../admin/public/dist/css/AdminLTE.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="../admin/public/dist/css/skins/_all-skins.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="../admin/public/plugins/iCheck/flat/blue.css">
        <!-- Morris chart -->
        <link rel="stylesheet" href="../admin/public/plugins/morris/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="../admin/public/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="../admin/public/plugins/datepicker/datepicker3.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="../admin/public/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
        <!-- Pace style -->
        <link rel="stylesheet" href="../admin/public/plugins/pace/pace.min.css">
        <!-- Own stylesheet -->
        <link rel="stylesheet" href="../admin/public/styles/style.css">
        <!-- select2-->
        <link rel="stylesheet" href="../admin/public/plugins/select2/select2.min.css">

        <link rel="stylesheet" href="<?php print(URL); ?>public/styles/style.css"/>

        <!--?php echo $stylesheets; ?-->
    </head>

