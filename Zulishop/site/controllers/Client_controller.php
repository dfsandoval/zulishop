<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Client_controller
 *
 * @author pabhoz
 */
class Client_controller extends BController{

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->view->client = Client_bl::getAll();
        $this->view->render($this,"index");
    }
    
    public function create(){
        $r = Client_bl::create($_POST);
        print(json_encode($r));
    }


}
