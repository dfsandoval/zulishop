<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Index_controller
 *
 * @author pabhoz
 */
class Perfil_controller extends Controller{
   
    function __construct() {
        parent::__construct();
    }

    public function index()
    {
        $this->view->debug = true;
        $this->view->cliente= Client_bl::getAll();
        //$usrCtrlr = new Users_controller();
        //$this->view->usrCtrlr = $usrCtrlr;
    
        $this->view->render($this,"index","ZuliShop | Perfil");
    }
    
}
